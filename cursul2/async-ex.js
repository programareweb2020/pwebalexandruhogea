(async () => {
    const f = async () => { return 1; }
    f().then(res => console.log(res));

    const result = await Promise.resolve(100);
    console.log(result);

    const buggyF = async () => {
        throw new Error('Oups!');
    }

    try {
        await buggyF();
    } catch (e) {
        console.trace(e);
    }
})();
