const number = 3;
console.log(typeof number);

const string = "string";
console.log(typeof string);

const boolean = true;
console.log(typeof boolean);

const notDefined = undefined;
console.log(typeof notDefined);

const simbol = Symbol();
console.log(typeof simbol);

const myFunc = () => console.log('Hello');
console.log(typeof myFunc);

const obj = {a:'b'};
console.log(typeof obj);

const arr = [1, 2, 3, 4];
console.log(typeof arr);

const nil = null;
console.log(typeof nil);
