const DELAY = 2000;
const INTERVAL = 1000;
const posibilitati = ['azi', 'niciodata'];
const maApucDeInvatat = new Promise((resolve, reject) => {
    setTimeout(() => {
        const cand = posibilitati[Math.floor(Math.random() * 2)];
        switch (cand) {
            case 'azi':
                resolve(cand);
            case 'niciodata':
                reject(new Error('Restanta!'));
        }
    }, DELAY);
});
const interval = setInterval(() => {
        console.log(maApucDeInvatat);
}, INTERVAL);
maApucDeInvatat
    .then(cand => {
        clearInterval(interval);
        console.log(`Ma apuc de invatat de ${cand}`);
    })
    .catch(err => {
        clearInterval(interval);
        console.trace(err);
    });

