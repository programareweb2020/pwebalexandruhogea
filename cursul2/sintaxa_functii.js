function conventionalFunc (param1, param2) {
    return param1 + param2;
};

const awesomeArrowFunc = (param1, param2) => {
    return param1 + param2;
};
