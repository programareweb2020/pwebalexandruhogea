const promise1 = Promise.resolve(1);
const promise2 = Promise.resolve(2);
const promise3 = new Promise((resolve, reject) => {
    setTimeout(() => resolve(100), 1000);
});
const results = Promise.all([
    promise1,
    promise2,
    promise3
]);
console.log(results);
results.then(actualResults => console.log(actualResults));

