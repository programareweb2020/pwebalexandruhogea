function Curs(materie, an, specializare) {
    this.materie = materie;
    this.an = an;
    this.specializare = specializare;
    this.medie = function () {
        return Math.floor(Math.random() * 11);
    }
    this.toString = function() {
        return `Cursul ${this.materie} din anul ${this.an} la specializarea ${specializare}`;
    }
}
const curs = new Curs('Programare Web', '4', 'C5');

console.log(curs.toString());
console.log(`Voi lua media ${curs.medie()}`);
