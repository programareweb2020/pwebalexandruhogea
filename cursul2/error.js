const buggyFunction = () => {
    const err = new Error('Something bad happened');
    err.name = 'FancyError';
    err.fancyStatus = 'fancy status'; //camp nou
    throw err;
}

try {
    buggyFunction();
} catch(e) {
    console.log(`Am primit eroarea ${e.name} cu campul custom fancyStatus = ${e.fancyStatus}`);
    console.log(e.stack); //sau console.trace(e);
}
