const obj1 = {a:2};

const obj2 = obj1; // copiere prin referinta 
obj2.a = 5;
console.log(`Obj1 este ${JSON.stringify(obj1)}`);
console.log(`Obj2 este ${JSON.stringify(obj2)}`);
 
const obj3 = Object.assign({}, obj1); // copiere prin valoare
obj3.a = 10;
console.log(`Obj1 este ${JSON.stringify(obj1)}`);
console.log(`Obj3 este ${JSON.stringify(obj3)}`);

const obj4 = {...obj1} // spread operator
obj4.a = 9772;
console.log(`Obj1 este ${JSON.stringify(obj1)}`);
console.log(`Obj4 este ${JSON.stringify(obj4)}`);

const obj6 = JSON.parse(JSON.stringify(obj1)); // cea mai buna varianta
obj6.a = 'mere';
console.log(`Obj1 este ${JSON.stringify(obj1)}`);
console.log(`Obj6 este ${JSON.stringify(obj6)}`);


