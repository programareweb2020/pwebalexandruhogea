const DELAY = 1000; //milisecunde

const cb1 = () => {
    console.log(`Salut din callback dupa ${DELAY} ms!`)
};
setTimeout(cb1, DELAY);

this.x = 1;
this.y = 1;
const sum = (x, y) => { return x + y };
const getThis = () => {
    return this;
}
const cb2 = (cb1, cb2) => {
    const obj = cb1();
    let a = obj.x;
    let b = obj.y;
    obj.x = cb2(a, b);
    obj.y = cb2(obj.x, b);
    console.log(`x: ${obj.x} si y: ${obj.y}`);
}
setInterval(() => cb2(getThis, sum), DELAY);

