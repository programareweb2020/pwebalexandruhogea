this.name = 'Darth';
this.surname = 'Vader';

const arrowPerson = {
    name: 'Carlos',
    surname: 'Sageata',
    fullName: function () {
        console.log(`Full name is ${this.name} ${this.surname}`);
    }
};

const conventionalPerson = {
    name: 'Ion',
    surname: 'Popescu',
    fullName: function () {
        // console.log(`Full name is ${this.name} ${this.surname}`);
        arrowPerson.fullName();
    },
}
conventionalPerson.fullName();

// conventionalPerson.fullName();
// arrowPerson.fullName();

