const simbol = Symbol();
const key = 'prenume';

const obj = {
    [simbol]: 'first value',
    key: 'second value',
    [key]: 'third value'

};

console.log(obj);
console.log(obj[simbol]);
console.log(Object.keys(obj));

