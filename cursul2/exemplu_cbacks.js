const funcA = () => console.log('A');
const funcB = () => console.log('B');
const funcG = () => console.log('G');

const funcF = (cb) => {
    cb();
    console.log('F');
};
const funcE = (cb) => {
    cb(funcG);
    console.log('E');
};
const funcC = (cb) => {
    cb(funcF);
    console.log('C');
};
const funcD = () => console.log('D');

funcA();
funcB();
funcC(funcE);
funcD();